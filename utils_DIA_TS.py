import jtextfsm as textfsm
import ipaddress

# Granite's public ip subnets
PUBLIC_IP_SUBNETS = ['172.85.128.0/17',
                     '134.6.0.0/16',
                     '134.204.0.0/17',
                     '143.170.0.0/16']


def extract_version_info_from_out(out):
    with open('textfsm/show_version.textfsm') as parsing_template:
        re_table = textfsm.TextFSM(parsing_template)
    out_parsed = re_table.ParseText(out)
    version_info = {
        'ios_version': out_parsed[0][0],
        'uptime': out_parsed[0][1],
        'model': out_parsed[0][2]
    }
    return version_info


def extract_interfaces_info_from_out(out):
    with open('textfsm/show_interfaces.textfsm') as parsing_template:
        re_table = textfsm.TextFSM(parsing_template)
    out_parsed = re_table.ParseText(out)
    interfaces_info = {}
    for item in out_parsed:
        interface = item[0] + item[1]
        interfaces_info[interface] = {
            'status': item[2],
            'protocol_status': item[3],
            'ip': item[4],
            'duplex': item[5],
            'speed': item[6],
            'media_type': item[7],
            'error_history': {
                'input_transfer_rate': item[8],
                'output_transfer_rate': item[9],
                'runts': item[10],
                'giants': item[12],
                'throttles': item[12],
                'input_errors': item[13],
                'crc': item[14],
                'frame': item[15],
                'overrun': item[16],
                'ignored': item[17],
                'watchdog': item[18],
                'multicast': item[19],
                'pause_input': item[20],
                'underruns': item[21],
                'output_errors': item[22],
                'collisions': item[23],
                'interface_resets': item[24],
                'unknown_protocol_drops': item[25],
                'babbles': item[26],
                'late_collision': item[27],
                'deferred': item[28],
                'lost_carrier': item[29],
                'no_carrier': item[30],
                'pause_output': item[31],
                'output_buffer_failures': item[32],
                'output_buffers_swapped_out': item[33]
                }
        }
    return interfaces_info


# value of model should be either 'ASR920' or 'ISR4000'
def extract_fiber_info_from_out(out, model):
    fiber_info = {}
    if model == 'ASR920':
        with open('textfsm/fiber_ASR920.textfsm') as parsing_template:
            re_table = textfsm.TextFSM(parsing_template)
        out_parsed = re_table.ParseText(out)
        for item in out_parsed:
            fiber_info[item[0]] = {
                'temperature': item[1],
                'tx': item[2],
                'rx': item[3]
            }
    if model == 'ISR4000':
        with open('textfsm/fiber_ISR4000.textfsm') as parsing_template:
            re_table = textfsm.TextFSM(parsing_template)
        out_parsed = re_table.ParseText(out)
        fiber_info = {
            'temperature': out_parsed[0][0],
            'tx': out_parsed[0][1],
            'rx': out_parsed[0][2]
        }
    return fiber_info


def extract_ping_info_from_out(out):
    with open('textfsm/ping.textfsm') as parsing_template:
        re_table = textfsm.TextFSM(parsing_template)
    out_parsed = re_table.ParseText(out)
    ping_info = {
        'success_rate': out_parsed[0][0],
        'avg': out_parsed[0][1]
    }
    return ping_info


def get_public_interface_ips(show_interfaces_out):
    interfaces = extract_interfaces_info_from_out(show_interfaces_out)
    public_interface_ips = {}
    for interface in interfaces:
        ip = interfaces[interface]['ip']
        if ip == '':
            continue
        interface_ip = ipaddress.ip_address(ip)
        for ip_subnet in PUBLIC_IP_SUBNETS:
            network = ipaddress.ip_network(ip_subnet)
            if interface_ip in network:
                public_interface_ips[interface] = ip
                break
    return public_interface_ips


class NOT_ASR920_OR_ISR400_DEVICE_ERROR(Exception):
    def __init__(self, hostname):
        self.hostname = hostname

    def __str__(self):
        return 'Host ' + self.hostname + ' NOT_ASR920_OR_ISR4000_DEVICE Error: Request denied'


class CUSTOMER_ERROR(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg
